##
## DecentCI Infrastructure
##
infrastructure:
  argocd:
    enabled: true
    project: decentci
    sidecar:
      enabled: true
      project: decentci
      repoURL: https://gitlab.com/decentci/deployment.git
      path: "apps/decentci/sidecars/argocd-sidecar"
      targetRevision: "main"
    values:
      ingress:
        hostname: "decentci.ncsa.illinois.edu"
      argo-cd:
        server:
          config:
            url: "https://decentci.ncsa.illinois.edu/argo-cd"
            dex.config: |
              connectors:
                - type: gitlab
                  id: gitlab-decentci
                  name: GitLab DecentCI
                  config:
                    # Set these values in `argocd-secret` Secret
                    clientID: $dex.gitlabcircuses.clientId
                    clientSecret: $dex.gitlabcircuses.clientSecret
                    groups:
                    - "decentci"
                # - type: gitlab
                #   id: gitlab
                #   name: GitLab
                #   config:
                #     # Set these values in `argocd-secret` Secret
                #     clientID: $dex.gitlab.clientId
                #     clientSecret: $dex.gitlab.clientSecret
                #     groups:
                #     # https://gitlab.com/antares-at-ncsa
                #     - "antares-at-ncsa"
        configs:
          secret:
            createSecret: false
  sealedsecrets:
    enabled: true
    project: decentci
  certmanager:
    enabled: true
    project: decentci
    argoapp:
      namespace: argo-cd
    values:
      clusterIssuer:
        email: "manninga@illinois.edu"
  traefik:
    enabled: true
    project: decentci
    values:
      service:
        ## List of external IP addresses
        externalIP:
        - 141.142.219.62
        spec:
          loadBalancerIP: "192.168.0.53"
      additionalArguments:
      - --providers.kubernetesingress.ingressendpoint.ip=141.142.219.62
  metallb:
    enabled: true
    project: decentci
    values:
      IPAddressPool:
        addresses:
          - 192.168.0.53/32
  longhorn:
    enabled: true
    project: decentci
    values:
      defaultSettings:
        backupTarget: "nfs://taiga-nfs.ncsa.illinois.edu:/taiga/ncsa/radiant/bbgl/cluster-decentci/backups/longhorn"
  nfsprovisioner:
    enabled: true
    project: decentci
    values:
      storageClass:
        name: nfs-taiga
      nfs:
        server: "taiga-nfs.ncsa.illinois.edu"
        path: "/taiga/ncsa/radiant/bbgl/cluster-decentci/pv/nfs-taiga"
  keel:
    enabled: true
##
## DecentCI Services
##
services:
  backups:
    enabled: false
    project: decentci
    values:
      server:
        monitor:
          annotations: false
  hedgedoc:
    enabled: false
  keycloak:
    enabled: true
    project: decentci
    argoapp:
      namespace: argo-cd
    sidecar:
      enabled: true
      project: decentci
      repoURL: https://gitlab.com/decentci/deployment.git
      path: "apps/decentci/sidecars/keycloak-sidecar"
      targetRevision: "main"
    values:
      keycloak:
        httpRelativePath: "/auth/"
        ingress:
          hostname: "keycloak.antares.ncsa.illinois.edu"
          extraTls:
          - hosts:
            - "keycloak.antares.ncsa.illinois.edu"
            secretName: "keycloak-antares-ncsa-illinois-edu-tls"
  wordpress:
    enabled: false
    project: decentci
    sidecar:
      enabled: false
      project: decentci
      repoURL: https://gitlab.com/decentci/deployment.git
      path: "apps/decentci/sidecars/wordpress-sidecar"
      targetRevision: "main"
    values:
      wordpress:
        existingSecret: "wordpress-admin"
        wordpressBlogName: "DecentCI"
        smtpHost: "outbound-relays.techservices.illinois.edu"
        smtpPort: "25"
        smtpFromEmail: no-reply@illinois.edu
        replicaCount: 3
        ingress:
          enabled: true
          hostname: "decentci.ncsa.illinois.edu"
          extraTls:
          - hosts:
            - "decentci.ncsa.illinois.edu"
            secretName: "decentci-ncsa-illinois-edu-tls"
        mariadb:
          auth:
            existingSecret: "wordpress-db"
  discourse:
    enabled: false
    project: decentci
    sidecar:
      enabled: false
      project: decentci
      repoURL: https://gitlab.com/decentci/deployment.git
      path: "apps/decentci/sidecars/discourse-sidecar"
      targetRevision: "main"
    values:
      discourse:
        auth:
          email: "manninga@illinois.edu"
        host: "forum.decentci.ncsa.illinois.edu"
        siteName: 'DecentCI'
        replicaCount: 2
        ingress:
          hostname: "forum.decentci.ncsa.illinois.edu"
          extraTls:
          - hosts:
              - "forum.decentci.ncsa.illinois.edu"
            secretName: "forum-decentci-ncsa-illinois-edu-tls"
  nextcloud:
    enabled: false
    project: decentci
    sidecar:
      enabled: false
      project: decentci
      repoURL: https://gitlab.com/decentci/deployment.git
      path: "apps/decentci/sidecars/nextcloud-sidecar"
      targetRevision: "main"
    values:
      nextcloud:
        #########################################################################
        ## When upgrading Nextcloud, use delayed startup probe and single replica
        ##
        # startupProbe:
        #   enabled: true
        #   initialDelaySeconds: 600
        #   periodSeconds: 60
        #   timeoutSeconds: 60
        #   failureThreshold: 30
        #   successThreshold: 1
        # replicaCount: 1
        ##
        #########################################################################
        replicaCount: 2
        ingress:
          tls:
          - secretName: "cloud-decentci-ncsa-illinois-edu-tls"
            hosts:
            - "cloud.decentci.ncsa.illinois.edu"
        nextcloud:
          host: cloud.decentci.ncsa.illinois.edu
